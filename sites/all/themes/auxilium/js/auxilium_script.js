
var defaultPosition = true;
var tijdClosed = true;
var ervaringClosed = true;
var isOpen = false;
(function($) {

  Drupal.behaviors.auxilium_theme = {
    attach: function(context) {
      Drupal.behaviors.auxilium_theme.attachampmActions();
    },
    attachampmActions: function() {
      function Scrolldown() {
        $("html, body").animate({scrollTop: $('body').height() / 14}, "slow");
        $('#TijdTekst').hide();
        $('.navbar-nav').hide();
        $('#ervaringText').hide();
        $('.first a').unbind('click');
        $('.first a').click(function() {
          return false;
        });

        var a = document.createElement('a');
        var linkText = document.createTextNode("GRONDIG ANDERS");
        a.appendChild(linkText);
        a.title = "GRONDIG ANDERS";
        a.href = "#";
        a.id = "grondig";
        $('.first').append(a);
        $('#grondig').unbind('click');
        $('#grondig').click(function() {
          return false;
        });
        $('.first a:first-child').attr('id', 'administratie');
        $('.last a').unbind('click');
        $('.last a').click(function(){
           $("html, body").animate({scrollTop: $('#block-block-4').position().top-70}, "slower");
           return false;
        });
        $('#navbar .container').find( "ul" ).eq(1).css( "background-color", "red" );
        $('#navbar .container li:gt(1)').first().find("a").unbind('click');
        $('#navbar .container li:gt(1)').first().find("a").click(function(){
           $("html, body").animate({scrollTop: $('#ervaringFoto').position().top-70}, "slower");
           return false;
        });
      }
      window.onload = Scrolldown;

      $('.navbar-default .navbar-toggle').click(function() {
        if (defaultPosition) {
          $(".navbar-default .navbar-toggle").animate({"right": "295px"}, "slow");
          $('.navbar-nav').fadeIn("slow");
          $('.navbar-nav').show(3000);
          $(".navbar-default .navbar-toggle").css({'background-image': 'url(/sites/all/themes/auxilium/images/closeNav.png)'});
          defaultPosition = false;
        }
        else {
          defaultPosition = true;
          $(".navbar-default .navbar-toggle").animate({"right": "10%"}, "slow");
          $('.navbar-nav').fadeOut("slow");
          $('.navbar-nav').hide(3000);
          $(".navbar-default .navbar-toggle").css({'background-image': 'url(/sites/all/themes/auxilium/images/menuClosed.png)'});
        }

      });
      $('#TijdVakFoto').click(function() {
      $("html, body").animate({scrollTop: $('#TijdVakFoto').position().top-70}, "slower");
        /*
         if (tijdClosed) {
         $("html, body").animate({scrollTop: $('#TijdVakFoto').height() + 170}, "fast");
         var src = $('#TijdVakFoto').attr("src").replace("AuxiliumTijd.png", "TIjdDetail.png");
         $('#TijdVakFoto').attr("src", src);
         $('#TijdTekst').fadeIn("slow");
         var large = {height: "200px"};
         $('#TijdVakFoto').animate(large, "slower");
         tijdClosed = false;
         }
         else {
         var src = $('#TijdVakFoto').attr("src").replace("TIjdDetail.png", "AuxiliumTijd.png");
         $('#TijdVakFoto').attr("src", src);
         $('#TijdTekst').fadeOut("slower");
         var small = {height: "421px"};
         $('#TijdVakFoto').animate(small, "slower");
         tijdClosed = true;
         }
         */
      });



      $('#TijdVakFoto').mouseenter(function(e) {
        /*
         if (e.pageY < $('#TijdVakFoto').position().top + 400)
         {
         $("html, body").animate({scrollTop: $('#TijdVakFoto').height()}, "fast");
         
         }
         $("html, body").bind("scroll mousedown DOMMouseScroll mousewheel keyup", function() {
         $('html, body').stop();
         });
         
         $('html,body').animate({scrollTop: $(this).position().top}, 'slow', function() {
         $("html, body").unbind("scroll mousedown DOMMouseScroll mousewheel keyup");
         });
         e.stopPropagation();
         });
         /*
         $('#TijdVakFoto').on('mouseleave', function() {
         $(this).stop(true).animate({scrollTop: $('#TijdVakFoto').height() + 150}, "fast");
         */
      });


      $('#block-block-4').click(function(){
        $("html, body").animate({scrollTop: $('#block-block-4').position().top-70}, "slower");
      });
      $("#ilogo").click(function(){
        $("html, body").animate({scrollTop: 0}, "slower");
      });

      $("#ervaringFoto").click(function() {
        $("html, body").animate({scrollTop: $("#ervaringFoto").position().top-70}, "slower");
       
        /*
         if (ervaringClosed)
         {
         var src = $('#ervaringFoto').attr("src").replace("ervaring.png", "ervaringDetail.png");
         $('#ervaringFoto').attr("src", src);
         $("html, body").animate({scrollTop: $('#ervaringFoto').height() + 600}, "slower");
         $('#ervaringText').fadeIn("slow");
         var small = {height: "200px"};
         $('#ervaringFoto').animate(small);
         ervaringClosed = false;
         }
         else {
         var src = $('#ervaringFoto').attr("src").replace("ervaringDetail.png", "ervaring.png");
         $('#ervaringFoto').attr("src", src);
         $('#ervaringText').fadeOut("slow");
         var large = {height: "421px"};
         $('#ervaringFoto').animate(large);
         ervaringClosed = true;
         }
         */
      });
      /*
       $("#ervaringFoto").mouseenter(function(e) {
       
       if (e.pageY < $('#ervaringFoto').position().top + 400)
       {
       $("html, body").animate({scrollTop: $('#ervaringFoto').height() + 460}, "slow");
       
       }
       $("html, body").bind("scroll mousedown DOMMouseScroll mousewheel keyup", function() {
       $('html, body').stop();
       });
       
       $('html,body').animate({scrollTop: $(this).position().top}, 'slow', function() {
       $("html, body").unbind("scroll mousedown DOMMouseScroll mousewheel keyup");
       });
       e.stopPropagation();
       });
       */
      $('.readmore').click(function() {
        if (isOpen) {
          $('.readmore').text("read more");
          var large = {height: "20px"};
          $('#OnzeTekst').animate(large, "slower");

          isOpen = false;
        }
        else {
          $('.readmore').text("read less");
          var large = {height: "200px"};
          $('#OnzeTekst').animate(large, "slower");

          isOpen = true;
        }
        /*alert(isOpen);*/

      });
    }


  };
})(jQuery);
